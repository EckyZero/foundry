import * as flatted from 'flatted';
import { wait } from '../../lib/timer';
import { Monster } from '../monsters/Monster';

const LOCATION_MAPPING = {
  '0': { x: -100, y: 100 },
  '1': { x: 0, y: 100 },
  '2': { x: 100, y: 100 },
  '3': { x: 100, y: 0 },
  '4': { x: 100, y: -100 },
  '5': { x: 0, y: -100 },
  '6': { x: -100, y: -100 },
  '7': { x: -100, y: 0 },
};

export async function place(
  monster: Monster,
  token: Token,
  count: number,
): Promise<void> {
  for (let i = 0; i < count; i++) {
    const summonedToken = safeDuplicate(token.actor.data.token);

    console.log('summonedToken: ', summonedToken);

    const [createdToken] = await game.scenes.viewed.createEmbeddedDocuments(
      'Token',
      [summonedToken],
      {
        animate: false,
      },
    );

    console.log('createdToken: ', createdToken);

    // await wait(500);

    const permission = safeDuplicate(createdToken.actor.data.permission);
    permission[game.user.id] = 3;
    const placement = placementForMonsterAtIndex(i);
    const updates = {
      'actorData.permission': permission,
      scale: token.data.scale,
      x: token.data.x + placement.x,
      y: token.data.y + placement.y,
      name: monster.name,
      img: monster.image_url,
      _id: createdToken.id,
    };

    await canvas.scene.updateEmbeddedDocuments('Token', [updates], {
      animate: false,
    });
    await canvas.animatePan({ x: token.data.x, y: token.data.y, scale: 1 });
  }
}

export function placementForMonsterAtIndex(index: number): {
  x: number;
  y: number;
} {
  switch (index) {
    case 0:
      return { x: -100, y: 100 };
    case 1:
      return { x: 0, y: 100 };
    case 2:
      return { x: 100, y: 100 };
    case 3:
      return { x: 100, y: 0 };
    case 4:
      return { x: 100, y: -100 };
    case 5:
      return { x: 0, y: -100 };
    case 6:
      return { x: -100, y: -100 };
    case 7:
      return { x: -100, y: 0 };
    default:
      return { x: 0, y: 0 };
  }
}

function safeDuplicate(data: any): any {
  return flatted.parse(flatted.stringify(data));
}
