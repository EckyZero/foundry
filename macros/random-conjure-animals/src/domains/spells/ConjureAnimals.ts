import * as MonsterManuel from '../monsters/MonsterManual';
import { GoodAlignments, NeutralAlignments } from '../monsters/Alignment';
import { Environment } from '../monsters/Environment';
import { Monster } from '../monsters/Monster';
import { ChallengeRating } from '../monsters/ChallengeRating';

type ConjureAnimalResult = {
  count: number;
  monster?: Monster;
};

export const UNALLOWED_SOURCES = ['ToB'];

export const VALID_CHALLENGE_RATINGS = [
  ChallengeRating.OneFourth,
  ChallengeRating.OneHalf,
  ChallengeRating.One,
  ChallengeRating.Two,
];

export async function cast(
  environment: Environment,
): Promise<ConjureAnimalResult> {
  const result: ConjureAnimalResult = {
    monster: undefined,
    count: 0,
  };
  const monsters = await MonsterManuel.getMonsters();
  const monster = monsters
    .whereTypeIs('Beast')
    .whereAlignmentIsIn([...GoodAlignments, ...NeutralAlignments])
    .whereEnvironmentIs(environment)
    .whereChallengeRatingIsIn(VALID_CHALLENGE_RATINGS)
    .whereIsNotASwarm()
    .whereIsNotADinasour()
    .whereSourceIsNotIn(UNALLOWED_SOURCES)
    .selectRandom();

  if (!monster) return result;

  result.monster = monster;
  result.count = numMonstersPerChallengeRating(monster.challengeRating);

  return result;
}

export function numMonstersPerChallengeRating(cr: ChallengeRating): number {
  switch (cr) {
    case ChallengeRating.OneFourth:
      return 8;
    case ChallengeRating.OneHalf:
      return 4;
    case ChallengeRating.One:
      return 2;
    case ChallengeRating.Two:
      return 1;
    default:
      return 0;
  }
}
