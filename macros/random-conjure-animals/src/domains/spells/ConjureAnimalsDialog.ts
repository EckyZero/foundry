import '../../lib/extensions/array.extensions';
import '../../lib/models/Dialog';

import { Environment } from '../monsters/Environment';

type ConjureAnimalDialogResult = {
  environment: Environment;
  token: Token | undefined;
};

const environments = Object.keys(Environment).toHtmlSelectTag(
  'environment',
  'What environment are you in?',
);

export function prompt(): Promise<ConjureAnimalDialogResult> {
  return new Promise((resolve) => {
    const d = new Dialog({
      title: 'Random Conjure Animals',
      content: `
        <style>
            select {
                width: 100%;
                margin-bottom: 10px;
            }
        </style>
        <p>You call out to the gods of nature for aid!</p>
        <form>
            ${environments}
        </form>`,
      buttons: {
        submit: {
          icon: '<i class="fas fa-dice-d20"></i>',
          label: 'Random Select',
          callback: async (html: DialogResponse) => {
            const environment = html
              .find('#environment')[0]
              .value.toUpperCase() as Environment;
            const token = canvas.tokens.children[0].children.find((t) =>
              Object.keys(t.actor.data.permission).includes(game.userId),
            );
            resolve({ environment, token });
          },
        },
      },
      default: 'submit',
    });
    d.render(true);
  });
}
