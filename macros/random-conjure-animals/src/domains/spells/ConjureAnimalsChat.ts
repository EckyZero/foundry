import pluralize from 'pluralize';

import '../../lib/models/Chat';
import { Monster } from '../monsters/Monster';

export async function publish(
  monster: Monster,
  count: number,
  actor: Actor,
): Promise<void> {
  const verb = count === 1 ? 'comes' : 'come';
  const link = `<a href="${monster.detail_url}">${monster.plural_name}</a>`;
  const chatData = {
    user: game.user.id,
    speaker: ChatMessage.getSpeaker(),
    content: `${count} ${link} ${verb} to your aid!`,
  };
  await ChatMessage.create(chatData, {});
}
