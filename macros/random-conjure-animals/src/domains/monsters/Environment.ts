export enum Environment {
  Arctic = 'ARCTIC',
  Coastal = 'COASTAL',
  Desert = 'DESERT',
  Forest = 'FOREST',
  Grassland = 'GRASSLAND',
  Hill = 'HILL',
  Mountain = 'MOUNTAIN',
  Swamp = 'SWAMP',
  Underdark = 'UNDERDARK',
  Underwater = 'UNDERWATER',
  Urban = 'URBAN',
}
