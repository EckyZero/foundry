export enum Alignment {
  Any = 'Any',
  Unaligned = 'Unaligned',
  ChaoticEvil = 'CE',
  Evil = 'E',
  LawfulEvil = 'LE',
  NeutralEvil = 'NE',
  ChaoticNeutral = 'CN',
  Neutral = 'N',
  LawfulNeutral = 'LN',
  NeutralGood = 'NG',
  ChaoticGood = 'CG',
  Good = 'G',
  LawfulGood = 'LG',
}

export const GoodAlignments = [
  Alignment.Good,
  Alignment.LawfulGood,
  Alignment.NeutralGood,
  Alignment.ChaoticGood,
];

export const NeutralAlignments = [
  Alignment.ChaoticNeutral,
  Alignment.Neutral,
  Alignment.LawfulNeutral,
  Alignment.NeutralGood,
  Alignment.Any,
  Alignment.Unaligned,
];

export const EvilAlignments = [
  Alignment.ChaoticEvil,
  Alignment.Evil,
  Alignment.LawfulEvil,
  Alignment.NeutralEvil,
];
