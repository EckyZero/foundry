import { Alignment } from './Alignment';
import { Monster } from './Monster';
import { Environment } from './Environment';
import { ChallengeRating } from './ChallengeRating';

export default class MonsterList extends Array<Monster> {
  constructor(items?: Array<Monster>) {
    if (items?.length) super(...items);
    else super();
    Object.setPrototypeOf(this, Object.create(MonsterList.prototype));
  }
  whereEnvironmentIs(environment: Environment): MonsterList {
    return this.filter((m) =>
      m.environments.includes(environment),
    ) as MonsterList;
  }

  whereSourceIsNotIn(sources: string[]): MonsterList {
    return this.filter((m) =>
      sources.some((s) => !m.bookSource.includes(s)),
    ) as MonsterList;
  }

  whereTypeIs(type: string): MonsterList {
    return this.filter(
      (m) => m.type.toLowerCase() === type.toLowerCase(),
    ) as MonsterList;
  }

  whereAlignmentIsIn(alignments: Alignment[]): MonsterList {
    return this.filter((m) =>
      alignments.some((a) => a === m.alignment),
    ) as MonsterList;
  }

  whereChallengeRatingIsIn(crs: ChallengeRating[]): MonsterList {
    return this.filter((m) =>
      crs.some((cr) => cr === m.challengeRating),
    ) as MonsterList;
  }

  whereIsNotADinasour(): MonsterList {
    return this.filter(
      (m) => !m.tag.toLowerCase().includes('dino'),
    ) as MonsterList;
  }

  whereIsNotASwarm(): MonsterList {
    return this.filter(
      (m) => !m.name.toLowerCase().includes('swarm'),
    ) as MonsterList;
  }

  selectRandom(): Monster | null {
    if (!this.length) return null;
    const min = 0;
    const max = this.length - 1;
    const random = Math.floor(Math.random() * (max - min + 1) + min);
    return this[random];
  }
}
