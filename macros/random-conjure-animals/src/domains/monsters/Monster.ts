import pluralize from 'pluralize';

import { Alignment } from './Alignment';
import { Environment } from './Environment';
import { ChallengeRating } from './ChallengeRating';

export class Monster {
  name: string;
  size: string;
  tag: string;
  type: string;
  alignment?: Alignment;
  environments: Environment[];
  challengeRating: ChallengeRating;
  bookSource: string;
  details?: MonsterDetail;

  get detail_url(): string {
    return `https://www.dndbeyond.com/monsters/${this.name.emptyCharsToDashChars()}`;
  }

  get image_url(): string {
    const formattedName = this.name.trimEmptyChars().trimDashChars();
    return `https://demo.foundryvtt.com/systems/dnd5e/tokens/beast/${formattedName}.png`;
  }

  get plural_name(): string {
    return pluralize(this.name);
  }

  constructor(data: Monster) {
    this.name = data.name;
    this.size = data.size;
    this.tag = data.tag;
    this.type = data.type;
    this.alignment = data.alignment;
    this.environments = data.environments;
    this.challengeRating = data.challengeRating;
    this.bookSource = data.bookSource;
    this.details = data.details;
  }
}

export interface MonsterDetail {
  index: string;
  name: string;
  size: string;
  type: string;
  subtype: string;
  armor_class: number;
  hit_points: number;
  hit_dice: string;
  speed: Speed;
  strength: number;
  dexterity: number;
  constitution: number;
  intelligence: number;
  wisdom: number;
  charisma: number;
  proficiencies: Proficiency[];
  damage_vulnerabilities: string[];
  damage_resistances: string[];
  damage_immunities: string[];
  condition_immunities: Condition[];
  senses: Senses;
  languages: string;
  special_abilities: Ability[];
  actions: Action[];
  url: string;
}

export interface Speed {
  walk?: string;
  fly?: string;
  swim?: string;
  burrow?: string;
  hover?: string;
  climb?: string;
}

export interface Senses {
  blindsight?: string;
  passive_perception?: number;
  darkvision?: string;
}

export interface Proficiency {
  proficiency: ObjectDetail;
  value: number;
}

export interface Condition {
  index: string;
  name: string;
  url: string;
  challenge_rating: number;
  xp: number;
}

export interface Ability {
  name: string;
  description: string;
}

export interface Action {
  name: string;
  desc: string;
  attack_bonus: number;
  damage: Damage[];
}

export interface Damage {
  damage_type: ObjectDetail;
  damage_dice: string;
}

export interface ObjectDetail {
  index: string;
  name: string;
  url: string;
}
