import '../../lib/extensions/string.extensions';

import { Monster, MonsterDetail } from './Monster';
import MonsterList from './MonsterList';
import { get } from '../../lib/ApiClient';

export const MONSTER_URL = `https://anemortalkid.github.io/all_monsters/data.json`;
export const MONSTER_DETAIL_URL = `https://www.dnd5eapi.co/api/monsters/`;

export async function getMonsters(): Promise<MonsterList> {
  const results = await get<Array<Monster>>(MONSTER_URL);
  return new MonsterList(results.map((m) => new Monster(m)));
}

export async function getMonsterDetail(
  monster: Monster,
): Promise<MonsterDetail> {
  const name = monster.name.emptyCharsToDashChars().toLowerCase();
  const url = MONSTER_DETAIL_URL + name;
  return get<MonsterDetail>(url);
}
