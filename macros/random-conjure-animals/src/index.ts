import * as ConjureAnimalsDialog from './domains/spells/ConjureAnimalsDialog';
import * as ConjureAnimalsChat from './domains/spells/ConjureAnimalsChat';
import * as ConjureAnimalsSpell from './domains/spells/ConjureAnimals';
import * as ConjureAnimalsCanvas from './domains/spells/ConjureAnimalsCanvas';
import * as flatted from 'flatted';

async function main(): Promise<void> {
  const { environment, token } = await ConjureAnimalsDialog.prompt();
  if (!environment || !token)
    return console.log('An error occurred prompting the user');
  const { monster, count } = await ConjureAnimalsSpell.cast(environment);
  if (!monster || !count)
    return console.log('An error occurred casting the spell');
  await ConjureAnimalsChat.publish(monster, count, token?.actor);
  await ConjureAnimalsCanvas.place(monster, token, count);
}

main();
