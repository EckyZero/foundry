export class ApiError extends Error {
  name = 'ApiError';
  url: string;
  constructor(url: string, message = 'error fetch resource') {
    super(message);
    this.url = url;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, Error.prototype);
  }
}
