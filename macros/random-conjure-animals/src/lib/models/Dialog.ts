declare class Dialog {
  options: DialogOptions;
  constructor(options: DialogOptions);
  render: (show: boolean) => void;
}

declare class DialogOptions {
  title: string;
  content: string;
  default: string;
  buttons: {
    submit: {
      icon: string;
      label: string;
      callback: (html: DialogResponse) => void;
    };
  };
}

declare class DialogResponse {
  find: (id: string) => FoundryHtmlElement[];
}

declare class FoundryHtmlElement {
  value: string;
}
