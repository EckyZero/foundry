declare class ChatMessage {
  static getSpeaker(): Actor;
  static create(data: ChatMessageData, context: any): void;
}

declare class ChatMessageData {
  user: string;
  speaker: Actor;
  content: string;
}
