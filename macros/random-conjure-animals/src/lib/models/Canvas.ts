declare class Canvas {
  tokens: TokenLayer;
  scene: Scene;

  animatePan(view: FoundryView): void;
}

declare class FoundryView {
  x: number;
  y: number;
  scale: number;
}

declare const canvas: Canvas;
