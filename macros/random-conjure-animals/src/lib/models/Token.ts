declare class TokenLayer {
  children: TokenGroup[];
}

declare class TokenGroup {
  children: Token[];
}

declare class Token {
  id: string;
  actor: Actor;
  data: TokenData;
}

declare class TokenData {
  x: number;
  y: number;
  scale: number;
}
