declare class Game {
  userId: string;
  user: User;
  scenes: Scenes;
}

declare class Scenes {
  viewed: Scene;
}

declare class FoundryDocument {
  collectionName: string;
}
declare class Scene {
  createEmbeddedDocuments(
    name: string,
    data: Array<any>,
    context?: DocumentModificationContext,
  ): Promise<Array<Token>>;

  updateEmbeddedDocuments(
    name: string,
    data: Array<any>,
    context?: DocumentModificationContext,
  ): Promise<Array<Token>>;
}

declare class DocumentModificationContext {
  animate: boolean;
}

declare const game: Game;
