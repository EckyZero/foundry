declare class Actor {
  data: ActorData;
  name: string;
  update(data: any): Promise<FoundryDocument>;
}

declare class ActorData {
  permission: any;
  token: Token;
}
