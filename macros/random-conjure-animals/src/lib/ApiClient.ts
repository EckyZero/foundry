import axios from 'axios';

import { ApiError } from './Errors';

const cache: {
  [key: string]: any;
} = {};

export async function get<T>(url: string): Promise<T> {
  if (cache[url]) return new Promise((resolve) => resolve(cache[url]));
  try {
    const response = await axios.get(url);
    const data = response.data as T;

    cache[url] = data;

    return data;
  } catch (e: unknown) {
    if (e instanceof Error) {
      throw new ApiError(url, e.message);
    }
    throw e;
  }
}
