declare global {
  interface Array<T> {
    toHtmlSelectTag(id: string, label: string): string;
  }
}

Array.prototype.toHtmlSelectTag = function (id: string, label: string): string {
  let html = `
    <label htmlFor='${id}'>${label}</label>
    <select id='${id}'>`;
  for (const value of this) {
    html += `\n<option value='${value}'>${value}</option>`;
  }
  html += `
    </select>`;
  return html;
};

export {};
