declare global {
  interface String {
    trimEmptyChars(): string;
    trimDashChars(): string;
    emptyCharsToDashChars(): string;
    parseUrl(): { domain: string; path: string };
  }
}

String.prototype.trimEmptyChars = function (): string {
  return replace(/ /g, String(this), '');
};

String.prototype.trimDashChars = function (): string {
  return replace(/-/g, String(this), '');
};

String.prototype.emptyCharsToDashChars = function (): string {
  return replace(/ /g, String(this), '-');
};

String.prototype.parseUrl = function (): { domain: string; path: string } {
  const url = new URL(String(this));
  return {
    domain: url.hostname,
    path: url.pathname,
  };
};

function replace(chars: RegExp, inWord: string, withChar: string): string {
  return inWord.replace(chars, withChar);
}

export {};
