/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['<rootDir>'],
  collectCoverageFrom: ['src/**/*.{ts,js}', '!src/**/*.d.ts'],
  globalSetup: './tests/test-setup.ts',
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
};
