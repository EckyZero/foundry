import nock, { Interceptor, Scope } from 'nock';

import {
  MONSTER_URL,
  MONSTER_DETAIL_URL,
} from '../../src/domains/monsters/MonsterManual';
import mockMonsters from './mockMonsters.json';
import mockMonsterDetail from './mockMonsterDetail.json';

export default {
  monsters: {
    mockSuccess: (): Scope => mock(MONSTER_URL).reply(200, mockMonsters),
    mockFailure: (): Scope => mock(MONSTER_URL).reply(404),
  },
  monsterDetail: {
    mockSuccess: (): Scope =>
      mock(MONSTER_DETAIL_URL).reply(200, mockMonsterDetail),
    mockFailure: (): Scope => mock(MONSTER_DETAIL_URL).reply(404),
  },
};

function mock(url: string): Interceptor {
  const { domain, path } = url.parseUrl();
  return nock(domain).get(path);
}
