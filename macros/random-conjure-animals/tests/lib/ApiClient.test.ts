import nock from 'nock';
import axios from 'axios';
import { expect, it, describe, afterEach, jest } from '@jest/globals';

import * as ApiClient from '../../src/lib/ApiClient';
import MockApi from '../mocks/mockApi';
import { MONSTER_URL } from '../../src/domains/monsters/MonsterManual';
import { Monster } from '../../src/domains/monsters/Monster';

describe('ApiClient', () => {
  const axiosSpy = jest.spyOn(axios, 'get');
  describe('get', () => {
    afterEach(() => {
      nock.cleanAll();
      axiosSpy.mockRestore();
    });
    it('should return results if the API response is 2xx', async () => {
      MockApi.monsters.mockSuccess();
      const monsters = await ApiClient.get<Array<Monster>>(MONSTER_URL);
      expect(monsters.length).toBe(1085);
    });
    it('should return cached results if a previous request was successful', async () => {
      await ApiClient.get<Array<Monster>>(MONSTER_URL);
      await ApiClient.get<Array<Monster>>(MONSTER_URL);

      expect(axiosSpy).toBeCalledTimes(0);
    });
    it('should throw an API Error if the API response is not 2xx', async () => {
      const url = 'http://test.com/testing';
      try {
        await ApiClient.get<Array<Monster>>(url);
        expect(true).toBe(false); // should fail
      } catch (e) {
        expect(e.name).toBe('ApiError');
        expect(e.url).toBe(url);
      }
    });
  });
});
