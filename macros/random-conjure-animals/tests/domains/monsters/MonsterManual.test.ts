import nock from 'nock';
import { expect, it, describe, afterEach } from '@jest/globals';

import MockApi from '../../mocks/mockApi';
import { getMonsters } from '../../../src/domains/monsters/MonsterManual';

describe('MonsterManual', () => {
  describe('getMonsters', () => {
    afterEach(() => {
      nock.cleanAll();
    });
    it('should return results as a MonsterList', async () => {
      MockApi.monsters.mockSuccess();
      const results = await getMonsters();

      expect(results.length).toBe(1085);
      expect(typeof results.whereAlignmentIsIn).toBe('function');
    });
  });
});
