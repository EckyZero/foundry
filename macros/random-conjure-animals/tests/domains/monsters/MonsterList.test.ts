import nock from 'nock';
import { expect, it, describe, afterEach, beforeEach } from '@jest/globals';

import MockApi from '../../mocks/mockApi';
import mockMonsters from '../../mocks/mockMonsters.json';
import MonsterList from '../../../src/domains/monsters/MonsterList';
import { Monster } from '../../../src/domains/monsters/Monster';
import { Environment } from '../../../src/domains/monsters/Environment';
import { Alignment } from '../../../src/domains/monsters/Alignment';
import { ChallengeRating } from '../../../src/domains/monsters/ChallengeRating';

describe('MonsterList', () => {
  const results = mockMonsters as Array<Monster>;
  const monsters = new MonsterList(results);
  beforeEach(() => {
    MockApi.monsters.mockSuccess();
  });
  afterEach(() => {
    nock.cleanAll();
  });
  describe('whereEnvironmentIs', () => {
    it('should only return monsters in an environment', async () => {
      const arctics = monsters.whereEnvironmentIs(Environment.Arctic);

      expect(arctics.every((m) => m.environments.includes(Environment.Arctic)));
      expect(arctics.length).toBe(69);
    });
  });
  describe('whereSourceIsNotIn', () => {
    it('should only return monsters not from one of many sources', async () => {
      const noTobs = monsters.whereSourceIsNotIn(['ToB']);

      expect(noTobs.every((m) => !m.bookSource.includes('ToB')));
      expect(noTobs.length).toBe(676);
    });
  });
  describe('whereTypeIs', () => {
    it('should only return monsters of type', async () => {
      const beasts = monsters.whereTypeIs('Beast');

      expect(beasts.every((m) => m.type === 'Beast'));
      expect(beasts.length).toBe(150);
    });
  });
  describe('whereAlignmentIsIn', () => {
    it('should only return monsters of matching one of many alignments', async () => {
      const goods = monsters.whereAlignmentIsIn([Alignment.Good]);

      expect(goods.every((m) => m.alignment === Alignment.Good));
      expect(goods.length).toBe(1);
    });
  });
  describe('whereChallengeRatingIsIn', () => {
    it('should only return monsters matching one of many challenge ratings', async () => {
      const crs = monsters.whereChallengeRatingIsIn([ChallengeRating.Six]);

      expect(crs.every((m) => m.challengeRating === ChallengeRating.Six));
      expect(crs.length).toBe(63);
    });
  });
  describe('whereIsNotADinasour', () => {
    it('should only return monsters that are not dinosaurs', async () => {
      const noDinos = monsters.whereIsNotADinasour();

      expect(noDinos.every((m) => !m.tag.includes('dino')));
      expect(noDinos.length).toBe(1068);
    });
  });
  describe('whereIsNotASwarm', () => {
    it('should only return monsters that are not swarms', async () => {
      const noSwarms = monsters.whereIsNotASwarm();

      expect(noSwarms.every((m) => !m.name.includes('swarm')));
      expect(noSwarms.length).toBe(1063);
    });
  });
  describe('selectRandom', () => {
    it('should select a monster at random', async () => {
      const monster1 = monsters.selectRandom();
      const monster2 = monsters.selectRandom();

      expect(monster1 === monster2).toBeFalsy();
    });
  });
});
