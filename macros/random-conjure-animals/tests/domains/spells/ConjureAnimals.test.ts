import { expect, it, describe, jest } from '@jest/globals';

import mockMonsters from '../../mocks/mockMonsters.json';
import MonsterList from '../../../src/domains/monsters/MonsterList';
import { Monster } from '../../../src/domains/monsters/Monster';

import {
  cast,
  numMonstersPerChallengeRating,
  VALID_CHALLENGE_RATINGS,
  UNALLOWED_SOURCES,
} from '../../../src/domains/spells/ConjureAnimals';
import { Environment } from '../../../src/domains/monsters/Environment';
import {
  GoodAlignments,
  NeutralAlignments,
} from '../../../src/domains/monsters/Alignment';

jest.mock('../../../src/domains/monsters/MonsterManual', () => ({
  getMonsters: (): Promise<MonsterList> => {
    const results = mockMonsters as Array<Monster>;
    return new Promise((resolve) => resolve(new MonsterList(results)));
  },
}));

describe('ConjureAnimals', () => {
  describe('cast', () => {
    it('should return a monster that meets the criteria', async () => {
      const { count, monster } = await cast(Environment.Forest);

      if (!monster) {
        throw Error('fail - "monster" is not defined');
      }

      expect(monster.type).toBe('Beast');
      expect([...GoodAlignments, ...NeutralAlignments]).toContain(
        monster.alignment,
      );
      expect(monster.environments).toContain(Environment.Forest);
      expect(VALID_CHALLENGE_RATINGS).toContain(monster.challengeRating);
      expect(monster.name.includes('Swarm')).toBeFalsy();
      expect(monster.tag.includes('Dino')).toBeFalsy();
      expect(UNALLOWED_SOURCES.includes(monster.bookSource)).toBeFalsy();

      expect(count).toBe(
        numMonstersPerChallengeRating(monster.challengeRating),
      );
    });
  });
});
