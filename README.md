# Foundry VTT Helpers

A collection of macros, modules, scripts, and other resources to improve the Foundry VTT experience

<span style="color:red">This repo is a work in progress</span>

## Macros

For development convenience, all Macros are written in TypeScript. In order to convert them to JavaScript macros supported by foundry, you need to do the folowing:

- Clone the repo locally
- Inside the macro subfolder that you're interested in (ex: `/macros/random-conjure-animals`) run the following commands
  - `npm compile` to generate the JavaScript files (placed in a `dist` directory)
  - `npm bundle` to aggeregate the JavaScript into a single file (placed in a `bundle` directory)
- Then copy-and-paste the bundled file directly into Foundry VTT!


### List of Macros

#### Random Conjure Animals

A fun homebrew way of treating the [Conjure Animals](https://www.dndbeyond.com/spells/conjure-animals). You select the environment you're in and the script will randomly select and place an animal native to that environment around you. Simulates your Druid/Ranger calling out to Mother Nature for help, and she sends whatever animals are nearby.


<figure class="video_container">
  <video controls="true" allowfullscreen="false" poster="./macros/random-conjure-animals/docs/random_conjure_animals.png">
    <source src="./macros/random-conjure-animals/docs/random_conjure_animals.mp4" type="video/mp4">
  </video>
</figure>